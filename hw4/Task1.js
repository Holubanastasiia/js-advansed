

/*

    Документация:
    
    https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
    
    form.checkValidity() > Проверка всех полей формы на валидость
    form.reportValidity() > Проверяет все поля на валидность и выводит сообщение с ошибкой

    formElement.validity > Объект с параметрами валидности поля 
    formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
        сообщения выведет message в браузерный попал

    Классы для стилизации состояния элемента
    input:valid{}
    input:invalid{}

    
    Задание:
    
    Используя браузерное API для валидации форм реализовать валидацию следующей формы:
    

    - Имя пользователя: type:text -> validation: required; minlength = 2;  
        Если пустое выввести сообщение: "Как тебя зовут дружище?!"
    - Email: type: email -> validation: required; minlength = 3; validEmail;
        Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
    - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
        Если пустой вывести сообщение: "Я никому не скажу наш секрет";
    - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
        Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
    - Напиши спасибо за яблоки: type: text -> validation: required; 
        Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

    - Согласен на обучение: type: checkbox -> validation: required;
        Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

    Внизу две кнопки:

    1) Обычный submit который отправит форму, если она валидна.
    2) Кнопка Validate(Проверить) которая запускает методы:
        - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
        - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

*/





var myForm = document.querySelector(".form");
var validate = document.querySelector('#Validate');
var submit = document.querySelector('#submit');


validate.addEventListener('click', function (e) {
    e.preventDefault();

    if (myForm.checkValidity() == true) {
        alert("form is correct");
    } else {
        alert('your form is filled incorrectly. check it one more time')
    }
});


submit.addEventListener('click', function () {
    var userName = document.querySelector('.userName');
    if (userName.value == '') {
        userName.setCustomValidity('Как тебя зовут дружище?!');
    } else {
        userName.setCustomValidity("");
    }

    var email = document.querySelector('.email');
    if (email.invalid) {
        email.setCustomValidity('Ну и зря, не получишь бандероль с яблоками!');
    } else {
        email.setCustomValidity("");
    }
    var password = document.querySelector('.password');
    if (password.value == '') {
        password.setCustomValidity('Я никому не скажу наш секрет');
    } else {
        password.setCustomValidity("");
    }
    var appleCounter = document.querySelector('.number');
    if (appleCounter.value <= '0') {
        appleCounter.setCustomValidity('ну хоть покушай немного... Яблочки вкусные')
    } else {
        appleCounter.setCustomValidity("");
    }

    var appreciation = document.querySelector('#appreciation')
    if (appreciation.value !== "спасибо") {
        appreciation.setCustomValidity('Фу, неблагодарный(-ая)!');
    } else {
        appreciation.setCustomValidity("");
    }

    var agreeCheckBox = document.querySelector('#agree');
    if (agreeCheckBox.checked !== true) {
        agreeCheckBox.setCustomValidity("Необразованные живут дольше! Хорошо подумай!")
    } else {
        agreeCheckBox.setCustomValidity("");
    }

});
