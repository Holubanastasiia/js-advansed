/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/

function Comment(name, messageText, avatarUrl) {
  this.name = name;
  this.messageText = messageText;
  if (avatarUrl) this.avatarUrl = avatarUrl;
  this.likes = 0;


}

Comment.prototype = {
  avatarUrl: './img/1.jpg',
  addLike: function () {
    this.likes++;
  }
}



var testComment1 = new Comment('Nastya', 'hello');
var testComment2 = new Comment('Oleg', 'hi', './img/2.jpg');
var testComment3 = new Comment('Egor', 'Ni Hao', './img/3.jpg');
var testComment4 = new Comment('Anna', 'Bonjour', './img/4.jpg');

let comments = [testComment1, testComment2, testComment3, testComment4];

let commentsContainer = document.getElementById('CommentsFeed');

function Avatar(comments) {
  this.comments = comments;

  this.Render = function () {
    this.comments.forEach(comment => {
      let block = document.createElement('div');

      let title = document.createElement('h3');
      title.innerText = comment.name;
      block.appendChild(title);

      let description = document.createElement('p');
      description.innerText = comment.messageText;
      block.appendChild(description);

      let avatar = document.createElement('img');
      avatar.src = comment.avatarUrl;
      block.appendChild(avatar);

      let likes = document.createElement('div');
      likes.className = 'like';
      likes.innerText = comment.likes;
      block.appendChild(likes);

      block.addEventListener('click', function () {
        comment.addLike();
        block.querySelector('.like').innerHTML = comment.likes;
        console.log(comment.likes)
      })

      commentsContainer.appendChild(block);
    })
  }

  this.Render();
}

let avatars = new Avatar(comments);
